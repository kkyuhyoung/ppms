/* *******************************************************
 * filename : layerpop.js
 * description : 모달레이어를 띄울때 사용되는 ajax JS
 * date : 2020-09-09
******************************************************** */


   function wrapWindowByMask() {
        //화면의 높이와 너비를 구한다.
        var maskHeight = $(document).height(); 
        var maskWidth = $(window).width();

        //문서영역의 크기 
        console.log( "document 사이즈:"+ $(document).width() + "*" + $(document).height()); 
        //브라우저에서 문서가 보여지는 영역의 크기
        console.log( "window 사이즈:"+ $(window).width() + "*" + $(window).height());        

        //마스크의 높이와 너비를 화면 것으로 만들어 전체 화면을 채운다.
        $('#mask').css({
		"margin-right":"px",
		"overflow-y":"hidden"
        });
		
		

        //애니메이션 효과
        //$('#mask').fadeIn(1000);      
        $('#mask').fadeTo("slow", 0.7);
    }

    function popupOpen() {
        $('.layerpop').css("position", "absolute");
        //영역 가운에데 레이어를 뛰우기 위해 위치 계산 
        $('.layerpop').css("top",(($(window).height() - $('.layerpop').outerHeight()) / 2) + $(window).scrollTop());
        $('.layerpop').css("left",(($(window).width() - $('.layerpop').outerWidth()) / 2) + $(window).scrollLeft());
        $('.layerpop').draggable();
        $('#layerbox').show();
    }

	function popupOpen2() {
        $('.layerpop').css("position", "absolute");
        //영역 가운에데 레이어를 뛰우기 위해 위치 계산 
        $('.layerpop').css("top",(($(window).height() - $('.layerpop').outerHeight()) / 2) + $(window).scrollTop());
        $('.layerpop').css("left",(($(window).width() - $('.layerpop').outerWidth()) / 2) + $(window).scrollLeft());
        $('.layerpop').draggable();
        $('#layerbox-down').show();
    }
    function popupOpensitemap() {
        $('.layerpopsitemap').css("position", "absolute");
        //영역 가운에데 레이어를 뛰우기 위해 위치 계산 
        $('.layerpopsitemap').css("top",(($(window).height() - $('.layerpopsitemap').outerHeight()) / 2) + $(window).scrollTop());
        $('.layerpopsitemap').css("left",(($(window).width() - $('.layerpopsitemap').outerWidth()) / 2) + $(window).scrollLeft());
        $('.layerpopsitemap').draggable();
        $('#layerbox-sitemap').show();
    }

    function popupClose() {
        $('#layerbox').hide();
        $('#mask').hide();
    }

    function popupClose2() {
        $('#layerbox-down').hide();
        $('#mask').hide();
    }
    function popupClosesitemap() {
        $('#layerbox-sitemap').hide();
        $('#mask').hide();
    }

    function goDetail() {


        /*팝업 오픈전 별도의 작업이 있을경우 구현*/ 

        popupOpen(); //레이어 팝업창 오픈 
        wrapWindowByMask(); //화면 마스크 효과 
    }

	function goDetail2() {

        /*팝업 오픈전 별도의 작업이 있을경우 구현*/ 

        popupOpen2(); //레이어 팝업창 오픈 
        wrapWindowByMask(); //화면 마스크 효과 
    }
	function goDetailsitemap() {

        /*팝업 오픈전 별도의 작업이 있을경우 구현*/ 

        popupOpensitemap(); //레이어 팝업창 오픈 
        wrapWindowByMask(); //화면 마스크 효과 
    }