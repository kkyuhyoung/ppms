/* *******************************************************
 * filename : sub.js
 * description : 서브컨텐츠에만 사용되는 JS
 * date : 2017-05-30
******************************************************** */

jQuery(function($){
	// alert('Hello World');
	// 회사연혁 탭
	if ( $(".product-tab-5col").length > 0 ) {
		$(window).scroll(function  () {
			var scroll_Top = $(window).scrollTop();
			var startTop = $(".page-business").offset().top;

			if ( scroll_Top > startTop ) {
				$(".product-tab-5col").addClass("fixed");
			}else {
				$(".product-tab-5col").removeClass("fixed");
			}
		
		
			var menuCount=$(".product-tab-5col li").size();
			var nav= new Array();
			for(var i=0;i<menuCount;i++){
				nav[i]="nav"+i;
				nav[i]=$($(".product-tab-5col li").eq(i).find("a").attr("href")).offset().top - 131;
			}
			
			$(".product-tab-5col li").each(function  (idx) {
				if( scroll_Top >= nav[idx] ){
					$('.product-tab-5col li').removeClass('on');
					$('.product-tab-5col li').eq(idx).addClass('on');
				};
			});
		});
		$(".product-tab-5col li a").click(function  () {
			$('.product-tab-5col li').removeClass('on');
			$(this).parent("li").addClass('on');
			var goDiv = $($(this).attr("href")).offset().top - 130;
			$("html, body").animate({scrollTop:goDiv},300,"swing");
			
			return false;
		});
	}
	

	/* FAQ */
    $(".faq-list-con .faq-item > dt").click(function  () {
        var $faqCon = $(this).siblings();
         
        if ($faqCon.css("display") == "block") {
            $(this).siblings().slideUp();
            $(".faq-item").removeClass("open");
             
        }else {
            $(".faq-item > dd:visible").slideUp();
            $(".faq-item").removeClass("open");
            $(this).parent("dl").addClass("open");
            $faqCon.slideDown();    
        }
    });

	/* 서브 pc-모바일 이미지 바꾸기 */
	function getScrollBarWidth(){
		if($(document).height() > $(window).height()){
			$('body').append('<div id="fakescrollbar" style="width:50px;height:50px;overflow:hidden;position:absolute;top:-200px;left:-200px;"></div>');
			fakeScrollBar = $('#fakescrollbar');
			fakeScrollBar.append('<div style="height:100px;">&nbsp;</div>');
			var w1 = fakeScrollBar.find('div').innerWidth();
			fakeScrollBar.css('overflow-y', 'scroll');
			var w2 = $('#fakescrollbar').find('div').html('html is required to init new width.').innerWidth();
			fakeScrollBar.remove();
			return (w1-w2);
		}
		return 0;
	}


	var w_width = $(window).outerWidth() + getScrollBarWidth();

	$(".chg-img").each(function  () {
		if ( w_width <= 800) {
			$(this).children("img").attr("src",$(this).children("img").attr("src").replace("_pc.jpg","_m.jpg"));
		}else {
			$(this).children("img").attr("src",$(this).children("img").attr("src").replace("_m.jpg","_pc.jpg"));
		}
	});

	
	$(".chg-img").each(function  () {
		if ( w_width <= 800) {
			$(this).children("img").attr("src",$(this).children("img").attr("src").replace("_pc_en.jpg","_m_en.jpg"));
		}else {
			$(this).children("img").attr("src",$(this).children("img").attr("src").replace("_m_en.jpg","_pc_en.jpg"));
		}
	});

$(".chg-img").each(function  () {
		if ( w_width <= 800) {
			$(this).children("img").attr("src",$(this).children("img").attr("src").replace("_pc_jp.jpg","_m_jp.jpg"));
		}else {
			$(this).children("img").attr("src",$(this).children("img").attr("src").replace("_m_jp.jpg","_pc_jp.jpg"));
		}
	});

$(".chg-img").each(function  () {
		if ( w_width <= 800) {
			$(this).children("img").attr("src",$(this).children("img").attr("src").replace("_pc_cn.jpg","_m_cn.jpg"));
		}else {
			$(this).children("img").attr("src",$(this).children("img").attr("src").replace("_m_cn.jpg","_pc_cn.jpg"));
		}
	});


	$(window).resize(function  () {
		var w_width = $(window).width();

	$(".chg-img").each(function  () {
			if ( w_width <= 800) {
				$(this).children("img").attr("src",$(this).children("img").attr("src").replace("_pc.jpg","_m.jpg"));
			}else {
				$(this).children("img").attr("src",$(this).children("img").attr("src").replace("_m.jpg","_pc.jpg"));
			}
		});

	$(".chg-img").each(function  () {
			if ( w_width <= 800) {
				$(this).children("img").attr("src",$(this).children("img").attr("src").replace("_pc_en.jpg","_m_en.jpg"));
			}else {
				$(this).children("img").attr("src",$(this).children("img").attr("src").replace("_m_en.jpg","_pc_en.jpg"));
			}
		});
	
		$(".chg-img").each(function  () {
			if ( w_width <= 800) {
				$(this).children("img").attr("src",$(this).children("img").attr("src").replace("_pc_jp.jpg","_m_jp.jpg"));
			}else {
				$(this).children("img").attr("src",$(this).children("img").attr("src").replace("_m_jp.jpg","_pc_jp.jpg"));
			}
		});
	
	$(".chg-img").each(function  () {
			if ( w_width <= 800) {
				$(this).children("img").attr("src",$(this).children("img").attr("src").replace("_pc_cn.jpg","_m_cn.jpg"));
			}else {
				$(this).children("img").attr("src",$(this).children("img").attr("src").replace("_m_cn.jpg","_pc_cn.jpg"));
			}
		});
	});
});
// JavaScript Document