<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>

<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>사후관리시스템</title>
<meta name="Subject" content="news">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 모바일사이트, 반응형사이트 제작시 사용 -->

<link rel="stylesheet" href="css/reset.css">
<link rel="stylesheet" href="css/common.css">

<link rel="stylesheet" href="css/layout.css">

<link rel="stylesheet" href="css/layout_responsive.css">

	
<link rel="stylesheet" href="css/board.css">
<!-- 게시판 제작시 사용 -->
<link rel="stylesheet" href="css/board_responsive.css">
<!-- 게시판(반응형, 모바일) 제작시 사용 --> 

<!-- 아이콘폰트 -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="css/fontawesome-free-5.14.0-web/css/all.css" rel="stylesheet">
	
 <!-- 용도모르는 스크립트 확인요망!!! -->	
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> 
<script>window.jQuery || document.write('<script src="http://code.jquery.com/jquery-1.8.3.min.js"><\/script>')</script>



<script src="js/common.js"></script> 
<script src="js/layer_popup.js"></script> 
<script src="js/jquery.validate.js"></script> 

<link rel="stylesheet" type="text/css" href="css/slick.css">

<script src="js/slick.js"></script> 

<!-- 스크롤 -->
<link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script> 
<script type="text/javascript" src="js/nav.js"></script> 

<script>
	$(function  () {
		dep1 = 04,
		dep2 = 01;
		if(dep1 > 0){
			$(".content-sub-tit").css("display", "none");
			$("#footer").addClass("sub");
		};
	})
</script>
	
	
<!-- 레이어팝업 스크립트 --> <link rel="stylesheet" href="css/layerpop.css">
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script> 
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script> 
<script src="js/layerpop.js"></script>
</head>


<body>

<!-- code -->
<div id="wrap"> 
  <!-- header -->
  <header id="header" class="fixed-header clearfix">
    <div class="gnb-overlay-bg"></div>
    <div id="gnbWrap" class="trans300">
      <div id="HeaderInner" class="clearfix">
        <h1 class="logo trans300"> <a href="#"> <img src="images/logo.png" width="185" height="41" alt="사후관리시스템"/> </a> </h1>
        <div class="header-util-box trans300 clearfix"> 
           <div class="header-lang trans300"> <span> <a  href="#">logout</a> </span> </div>
          <!-- 사이트맵 버튼 ( 기본 라인 三 ) -->
         <button onclick="javascript:goDetailsitemap('사이트맵');" class="sitemap-line-btn sitemap-open-btn" title="사이트맵 열기"> <span class="line line1"></span> <span class="line line2"></span> <span class="line line3"></span> </button>
          <!-- GNB Mobile --> 
          <a href="javascript;" class="nav-open-btn" title="네비게이션 열기"> <span class="line line1"></span> <span class="line line2"></span> <span class="line line3" ></span> </a> </div>
      </div>
      <nav id="gnb" class="each-menu">
		 
        <ul class="clearfix">
          <li class="dep-1  active on"> <a href="#">소식</a>
            <ul class="gnb-2dep" >
              <li> <a href="info_news.html"><span class="trans300"><em>공지사항</em></span></a> </li>
              <li> <a href="info_board.html"><span class="trans300"><em>자유게시판</em></span></a> </li>
              <li> <a href="info_faq.html"><span class="trans300"><em>FAQ</em></span></a> </li>
            </ul>
          </li>
          <li class="dep-1"> <a href="#">장애관리</a>
            <ul class="gnb-2dep" >
              <li> <a href="info_fault.html"><span class="trans300"><em>장애관리</em></span></a> </li>
            </ul>
          </li>
          <li class="dep-1"> <a href="#">예방점검</a>
            <ul class="gnb-2dep" >
              <li> <a href="info_preventive.html"><span class="trans300"><em>예방점검</em></span></a> </li>
            </ul>
          </li>
          <li class="dep-1"> <a href="#">교육자료관리</a>
            <ul class="gnb-2dep" >
              <li> <a href="info_video.html"><span class="trans300"><em>동영상자료</em></span></a> </li>
              <li> <a href="info_file.html"><span class="trans300"><em>문서자료</em></span></a> </li>
            </ul>
          </li>
          
        </ul>
      </nav>
      <div class="gnb-overlay-bg-m"></div>
      <nav id="gnbM" class="trans400" >
        <ul id="navigation">
          <li class="has-2dep ">  
			 <div class="header-lang-m " >홍길동님 환영합니다.  <span > <a  href="#">logout</a> </span>  </div>
			</li>
          <li class="has-2dep "> <a href="#">소식</a>
            <ul class="gnb-2dep">
              <li> <a href="info_news.html"><span class="trans300"><em>공지사항</em></span></a> </li>
              <li> <a href="info_board.html"><span class="trans300"><em>자유게시판</em></span></a> </li>
              <li> <a href="info_faq.html"><span class="trans300"><em>FAQ</em></span></a> </li>
            </ul>
          </li>
          <li class="has-2dep"> <a href="#">장애관리</a>
            <ul class="gnb-2dep">
              <li> <a href="info_fault.html"><span class="trans300"><em>장애관리</em></span></a> </li>
            </ul>
          </li>
          <li class="has-2dep"> <a href="#">예방점검</a>
            <ul class="gnb-2dep">
               <li> <a href="info_preventive.html"><span class="trans300"><em>예방점검</em></span></a> </li>
            </ul>
          </li>
          <li class="on has-2dep"> <a href="#">교육자료관리</a>
            <ul class="gnb-2dep">
              <li> <a href="info_video.html"><span class="trans300"><em>동영상자료</em></span></a> </li>
              <li> <a href="info_file.html"><span class="trans300"><em>문서자료</em></span></a> </li>
            </ul>
          </li>
         
        </ul>
      </nav>
    </div>
  </header>
   
  <!-- //header -->
  <!-- container -->
  <div id="container"> 
    <!-- visual -->
    
    <section id="visual" style="background:#1e2e47 url(images/008.jpg) no-repeat 50% 0%">
      <div class="area visual-txt-con">
        <h2 class="visual-tit trans400"> 동영상자료 </h2>
        <p class="visual-sub-txt">COMPANY INTRODUCTION</p>
      </div>
    </section>
    <!-- //visual --> 
    <!-- middleArea -->
    <div id="middleArea"> 
      <!-- 서브메뉴1 ( 메뉴나열(PC) + 모바일메뉴  )-->
      
   <aside id="sideMenu">
        <div class="side-menu-inner area">
          <ul id="subGnb" class="snb clearfix">
            <div class="nav-on-icon main-move-line"><!-- 움직이는 바 --> 
              <span style="overflow: hidden; left: 724.719px; width: 171px; display: none;"></span> </div>
            <li class="on"> <a href="#"><span class="trans300"><em>교육자료관리</em></span></a> </li>
            <li> <a href="#"><span class="trans300"><em>동영상자료</em></span></a> </li>
            <li> <a href="#"><span class="trans300"><em>문서자료</em></span></a> </li>
          </ul>
        </div>
      </aside>
      <aside id="sideMenuM" class="cm-top-menu clearfix">
        <div class="menu-location  location2"> <a href="javascript:;" class="cur-location"> <span>동영상자료</span> <i class="material-icons arrow"></i> </a>
          <ul class="location-menu-con">
            <li> <a href="info_video.html"><span class="trans300"><em>동영상자료</em></span></a> </li>
			<li class="on"> <a href="info_file.html"><span class="trans300"><em>문서자료</em></span></a> </li>
          </ul>
        </div>
      </aside>
      <!-- // --> 
      
      <!-- 상단정보 (센터정렬) -->
      <aside id="contentInfoCon" class="content-info-style01 trans400">
        <h3 class="content-tit rubik">문서자료</h3>
      </aside>
      <!-- content -->
      <section id="content" class="area"> 
        <!-- 컨텐츠 내용 --> 

        <!-- 문의 폼 시작 -->
       
        <form name="form" id="form" method="post" action="#" novalidate="novalidate">
          <input type="hidden" name="hl" value="kr">
          <div class="bbs-write-con">
            <article class="bbs-write-tbl-box">
              <table class="bbs-write-tbl">
                <caption>
                공지사항 작석폼
                </caption>
                <colgroup>
                <col style="width:20%;">
                <col>
                </colgroup>
                <tbody>
                  
					 <tr>
                    <th scope="row">구분</th>
                    <td>
						
						<select name="#" class="write-select width20" style=" margin-right: 5px">
                        <option value="HW">HW</option>
                        <option value="SW">SW</option>
                      </select>
					
						<select name="#" class="write-select width30 " >
                        <option value="관망관리시스템">관망관리시스템</option>
                        <option value="관망관리시스템">관망관리시스템</option>
                      </select>
						
				    </td>
                  </tr>
					
                  <tr>
                    <th scope="row">제목</th>
                    <td><input type="text" class="write-input width100" name="name" placeholder="제목을 입력해주세요"></td>
                  </tr>
                 
                  <tr>
                    <th scope="row">내용</th>
                    <td><textarea name="content" class="write-textarea"  placeholder="내용을 입력해주세요"></textarea></td>
                  </tr>
                  
                  <tr>
                    <th scope="row">파일</th>
                    <td class="file_input">
                      <input type="text" readonly="readonly" title="File Route" id="file_route"  placeholder="파일을 선택해주세요">
					  <label> 파일불러오기
                        <input type="file" onchange="javascript:document.getElementById('file_route').value=this.value">
                      </label></td>
                  </tr>
                </tbody>
              </table>
            </article>           
          </div>
        </form>
        <!-- // 문의 폼 끝 --> 		  
		   <div class="cm-btn-controls">
              <button type="submit" class="btn-style01" onClick="location.href='info_file.html'">저장</button>
              <button type="submit" class="btn-style01" onClick="location.href='info_file.html'">목록</button>
            </div>
		  
		
        <!-- //컨텐츠 내용 --> 
      </section>
		
      <!-- //content --> 
    </div>
    <!-- //middleArea --> 
  </div>
  <!-- //container --> 
  <!-- footer -->
  
  <footer id="footer" class="sub"> <a href="#wrap" class="to-top-btn"><i class="material-icons"></i></a>
    <div id="footerInner" class="clearfix">
      <article id="footerTop" class="">
        <div class="area-box clearfix">
          <div class="footer-info-cont"> <span class="copyright">© 2020 <em>Greentech INC</em> All rights reserved.</span> </div>
        </div>
      </article>
    </div>
  </footer>
  
  <!-- 모달 레이어팝업 -->
  <article class="modal-fixed-pop-wrapper">
    <div class="modal-fixed-pop-inner">
      <div class="modal-loading"><span class="loading"></span></div>
      <div class="modal-inner-box">
        <div class="modal-inner-content"> 
          <!-- ajax 내용 --> 
        </div>
      </div>
    </div>
  </article>
  <!-- //footer --> 
</div>
<!-- //code -->
	<!--sitemap Popup Start -->

	
	<article class="layerpop-cont">
	<div id="mask"></div>		  
    <div id="layerbox-sitemap" class="layerpopsitemap" style="width: 900px; height: 330px;">
        <article class="layerpop_area">
        <div class="title">사이트맵</div>
		
        <a href="javascript:popupClosesitemap();" class="layerpop_close" id="layerbox_close"><i class="material-icons"></i></a> 
        <div class="content">
 
         
          <article class="sitemap-wrapper">
		
		<ul >
         <li>
				<h2>소식</h2>
           <ul class="sitemap-2dep">
              <li> <a href="info_news.html"><span class="trans300"><em>공지사항</em></span></a> </li>
              <li> <a href="info_board.html"><span class="trans300"><em>자유게시판</em></span></a> </li>
              <li> <a href="info_faq.html"><span class="trans300"><em>FAQ</em></span></a> </li>
            </ul>
          </li>
          <li>
				<h2>장애관리</h2>
            <ul class="sitemap-2dep">
              <li> <a href="info_fault.html"><span class="trans300"><em>장애관리</em></span></a> </li>
            </ul>
          </li>
          <li>
				<h2>예방점검</h2>
           <ul class="sitemap-2dep">
              <li>  <a href="info_preventive.html"><span class="trans300"><em>예방점검</em></span></a>  </li>
            </ul>
          </li>
          <li>
				<h2>교육자료관리</h2>
            <ul class="sitemap-2dep">
                <li> <a href="info_video.html"><span class="trans300"><em>동영상자료</em></span></a> </li>
              <li> <a href="info_file.html"><span class="trans300"><em>문서자료</em></span></a> </li>
            </ul>
          </li>
			

          
        </ul>
		
		
	</article>
       
       
		  
    
        </div>
        </article>
    </div>
	</article>

    <!--sitemap Popup End -->

</body>
</html>