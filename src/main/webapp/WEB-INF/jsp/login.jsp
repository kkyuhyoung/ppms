<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
	<title>사후관리시스템</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">



	<link rel="stylesheet" type="text/css" href="css/main.css">
	<!-- 아이콘폰트 -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="css/fontawesome-free-5.14.0-web/css/all.css" rel="stylesheet">

<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" />
	<!-- 레이어팝업 스크립트 --> 
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script> 
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script> 
<script src="js/layerpop.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	//서버메세지체크
/*     var message = document.loginForm.message.value;
    if (!gfn_isNull(message)) {
        alert(message);
    } */
    //getid(document.loginForm);
	    $("#userid").focus();
	});

function actionLogin() {
 	if ($.trim($("#userid").val()) =="") {
		alert("아이디를 입력하세요");    
	    return;
	} else if ($.trim($("#userpw").val()) =="") {
	    alert("비밀번호를 입력하세요");
	    return;
	} else {
/* 		document.loginForm.action="<c:url value=''/>";
		document.loginForm.submit(); */
		
		 $.ajax({
			url : "/egovTest/Login.do",
			type : "post",
			data :{ "USER_ID" : $("#userid").val(),
					"USER_PWD" : $("#userpw").val(),
					"GBN" : "login"}
		})
		.done(function(jsonData){
			console.log(jsonData);
			if(jsonData.data != undefined){
				document.location.replace(jsonData.data.toString());	
			}else{
				$("#userpw").val("");
				$("#userpw").focus();
				alert("가입되지 않은 아이디이거나, 잘못된 비밀번호입니다.");
			}
		})
		// HTTP 요청이 실패하면 오류와 상태에 관한 정보가 fail() 메소드로 전달됨. 
		.fail(function(xhr, status, errorThrown) {
			$("#userid").html("오류가 발생했다.<br>")
			.append("오류명: " + errorThrown + "<br>")
			.append("상태: " + status); 
		}) 
		.always(function(xhr, status) {
			//$("#userid").val("요청이 완료되었습니다!"); 
		}); 
	}   
}
function searchID(){
 	if ($.trim($("#usertelid").val()) =="") {
		alert("전화번호를 입력하세요");    
	    return;
	}else{
		$.ajax({
			url : "/egovTest/SearchUserInfo.do",
			type : "post",
			data :{ "TEL" : $("#usertelid").val(),
					"GBN" : "searchid"}
		})
		.done(function(jsonData){
			
			if(jsonData.data.length ==1){
				let id = jsonData.data[0].USER_ID;
				var vava = funcMaskingID(id)
				alert("정보와 일치하는 아이디입니다.\n"+ vava);
			}else{
				alert("정보와 일치하는 아이디가 없습니다.");
			}
			//userData = data;
			//userData = JSON.parse(JSON.stringify(data));
			//$("#userid").val("요청이 완료되었습니다!"); 
		});
	}
}

function funcMaskingID(strID){
	let originID = strID;
	let maskingID; 
	let strLength;
	
	strLength = originID.length;
	if(strLength < 3){
		maskingID = originID.replace(/(?<=.{1})./gi, "*");
	}else {
		maskingID = originID.replace(/(?<=.{2})./gi, "*"); 
	} 
	
	return maskingID;
}

/* function setCookie (name, value, expires) {
    document.cookie = name + "=" + escape (value) + "; path=/; expires=" + expires.toGMTString();
}

function getCookie(Name) {
    var search = Name + "="
    if (document.cookie.length > 0) { // 쿠키가 설정되어 있다면
        offset = document.cookie.indexOf(search)
        if (offset != -1) { // 쿠키가 존재하면
            offset += search.length
            // set index of beginning of value
            end = document.cookie.indexOf(";", offset)
            // 쿠키 값의 마지막 위치 인덱스 번호 설정
            if (end == -1)
                end = document.cookie.length
            return unescape(document.cookie.substring(offset, end))
        }
    }
    return "";
}

function saveid(form) {
    var expdate = new Date();
    // 기본적으로 30일동안 기억하게 함. 일수를 조절하려면 * 30에서 숫자를 조절하면 됨
    if (form.checkId.checked)
        expdate.setTime(expdate.getTime() + 1000 * 3600 * 24 * 30); // 30일
    else
        expdate.setTime(expdate.getTime() - 1); // 쿠키 삭제조건
    setCookie("saveid", form.id.value, expdate);
}

function getid(form) {
    form.checkId.checked = ((form.id.value = getCookie("saveid")) != "");
} */
</script>
</head>
<body>
	<form id="loginForm" name="loginForm" class="login100-form validate-form flex-sb flex-w" method="post">
	<div class="limiter" >
		<div class="container-login100" >
			<div class="wrap-login100">
				
					<span class="login100-form-title">
						<img src="images/logo_login.png"  alt="사후관리시스템"/> 
					</span>

					<span class="txt1">
						아이디
					</span>
					<div class="wrap-input100 validate-input " data-validate = "Username is required">
						<input id="userid" value="" class="input100" type="text" name="userid" >
						<span class="focus-input100"></span>
					</div>
					
					<span class="txt1 p-b-11">
						비밀번호
					</span>
					<div class="wrap-input100 validate-input " data-validate = "Password is required">
						<span class="btn-show-pass">
							<i class="fa fa-eye"></i>
						</span>
						<input id="userpw"class="input100" type="password" name="userpw" onkeydown="javascript:if (event.keyCode == 13) { actionLogin(); }">
						<span class="focus-input100"></span>
					</div>
					
					<div class="flex-sb-m w-full ">
						<div class="contact100-form-checkbox">
							<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
							<label class="label-checkbox100" for="ckb1">
								아이디저장
							</label>
						</div>

						<div>
							<a href="javascript:goDetail('');" id="" class="txt3">
								비밀번호 찾기
							</a>
						</div>
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn" type="button" onClick="javascript:actionLogin()">
							로그인
						</button>
					</div>

				
			</div>
		</div>
	</div>
	</form>


<!-- Popup Start -->
<article class="layerpop-cont">
  <div id="mask"></div>
  <div id="layerbox" class="layerpop" >
    <article class="layerpop_area">
      <div class="title">ID / PW 찾기</div>
      <a href="javascript:popupClose();" class="layerpop_close" id="layerbox_close"><i class="material-icons"></i></a>
      <div class="content" > 
	
        <!-- 문의 폼 시작 -->

        <div class="layerpop-write-con-id" >
          <article class="layerpop-write-tbl-box">
            <table  class="layerpop-write-tbl"  >
				<caption>
             아이디찾기
              </caption>
				<thead><tr><td><i class="fas fa-user"></i> 아이디찾기</td></tr></thead>
              <tbody>
                <tr>
                  <th scope="row">전화번호</th>
                  <td><input id="usertelid" type="text" class="write-input width100" name="usertelid" onkeydown="javascript:if (event.keyCode == 13) { searchID(); }"></td>
                </tr>
                
				<tr><th style="height: 0"></th>
				  <td>
				  	<button type="submit" class="td_button " style="width: 100%" onClick="javascript:searchID()">
				  		찾기
				  	</button>
				  </td>
				</tr>
              </tbody>
            </table>
          </article>
        </div>
		  
        <div class="layerpop-write-con-pw" >
          <article class="layerpop-write-tbl-box">
			  
			   <table class="layerpop-write-tbl" >
              <caption>
             비밀번호찾기
              </caption>
             <thead><tr><td> <i class="fas fa-unlock"></i> 비밀번호찾기</td></tr></thead>
              <tbody >
                <tr>
                  <th scope="row">아이디</th>
                  <td><input type="text" class="write-input width70" name="name" ></td>
                </tr>
               <tr>
                  <th scope="row">전화번호</th>
                  <td><input id="usertelpw" type="text" class="write-input width100" name="name" ></td>
                </tr>
               <tr><th style="height: 0; padding: 20px 0 0 0;"></th>
				  <td><button type="submit" class="td_button " style="width: 100%">찾기</button></td></tr>
              </tbody>
            </table>
           
          </article>
        </div>
    
        <!-- // 문의 폼 끝 -->
        <div class="btn-controls" style="float: left; display: block; width: 100%; margin-top: 30px" >
        
          <button type="submit" class="btn-style01"  onClick="javascript:popupClose();">닫기</button>
        </div>
      </div>
    </article>
  </div>
</article>
<!--Popup End -->

</body>
</html>