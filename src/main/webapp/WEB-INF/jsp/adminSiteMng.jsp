<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>

<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>사후관리시스템</title>
<meta name="Subject" content="news">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 모바일사이트, 반응형사이트 제작시 사용 -->

<link rel="stylesheet" href="css/reset.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/layout_admin.css">
<link rel="stylesheet" href="css/board.css">
<link rel="stylesheet" href="css/layerpop.css">

<!-- 아이콘폰트 -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="css/fontawesome-free-5.14.0-web/css/all.css" rel="stylesheet">

<!-- 용도모르는 스크립트 확인요망!!! --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> 
<script>window.jQuery || document.write('<script src="http://code.jquery.com/jquery-1.8.3.min.js"><\/script>')</script> 
<script src="js/common.js"></script> 
<script src="js/layer_popup.js"></script> 
<script src="js/jquery.validate.js"></script>
<link rel="stylesheet" type="text/css" href="css/slick.css">
<script src="js/slick.js"></script> 

<!-- 스크롤 -->
<link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script> 
<script type="text/javascript" src="js/nav.js"></script> 
<script>
	$(function  () {
		dep1 = 04,
		dep2 = 01;
		if(dep1 > 0){
			$(".content-sub-tit").css("display", "none");
			$("#footer").addClass("sub");
		};

	})
</script> 

<!-- 레이어팝업 스크립트 --> 
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script> 
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script> 
<script src="js/layerpop.js"></script>
</head>

<body >
<div id="wrap"  > 
  <!-- header -->
  <header id="header" class="fixed-header clearfix">
    <div class="gnb-overlay-bg"></div>
    <div id="gnbWrap" class="trans300">
      <div id="HeaderInner" class="clearfix">
        <h1 class="logo trans300"> <a href="#"> <img src="images/logo.png" width="185" height="41" alt="사후관리시스템"/> </a> </h1>
        <div class="header-util-box trans300 clearfix">
          <div class="header-lang trans300"> <span> <a  href="#">logout</a> </span> </div>
          <!-- 사이트맵 버튼 ( 기본 라인 三 ) -->
            <button onclick="javascript:goDetailsitemap('사이트맵');" class="sitemap-line-btn sitemap-open-btn" title="사이트맵 열기"> <span class="line line1"></span> <span class="line line2"></span> <span class="line line3"></span> </button>
      </div>
      <nav id="gnb" class="each-menu">
        <ul class="clearfix">
         <li class="dep-1  active on"> <a href="#">소식</a>
            <ul class="gnb-2dep" >
              <li> <a href="info_news.html"><span class="trans300"><em>공지사항</em></span></a> </li>
              <li> <a href="info_board.html"><span class="trans300"><em>자유게시판</em></span></a> </li>
              <li> <a href="info_faq.html"><span class="trans300"><em>FAQ</em></span></a> </li>
            </ul>
          </li>
          <li class="dep-1"> <a href="#">장애관리</a>
            <ul class="gnb-2dep" style="display: none;">
              <li> <a href="info_fault.html"><span class="trans300"><em>장애관리</em></span></a> </li>
            </ul>
          </li>
          <li class="dep-1"> <a href="#">예방점검</a>
            <ul class="gnb-2dep" style="display: none;">
              <li> <a href="info_preventive.html"><span class="trans300"><em>예방점검</em></span></a> </li>
            </ul>
          </li>
          <li class="dep-1"> <a href="#">교육자료관리</a>
            <ul class="gnb-2dep" style="display: none;">
              <li> <a href="info_video.html"><span class="trans300"><em>동영상자료</em></span></a> </li>
              <li> <a href="info_file.html"><span class="trans300"><em>문서자료</em></span></a> </li>
            </ul>
          </li>
          <li class="dep-1"> <a href="#">관리자모드</a>
            <ul class="gnb-2dep" style="display: none;">
               <li> <a href="info_admin_site.html"><span class="trans300"><em>사이트관리</em></span></a> </li>
              <li> <a href="info_admin_user.html"><span class="trans300"><em>사용자관리</em></span></a> </li>
              <li> <a href="info_admin_code.html"><span class="trans300"><em>코드관리</em></span></a> </li>
              <li> <a href="info_admin_problem.html"><span class="trans300"><em>장애통계</em></span></a> </li>
            </ul>
          </li>
        </ul>
      </nav>
      <div class="gnb-overlay-bg-m"></div>
      <nav id="gnbM" class="trans400" >
        <ul id="navigation">
          <li class="has-2dep ">  
			 <div class="header-lang-m " >홍길동님 환영합니다.  <span > <a  href="#">logout</a> </span>  </div>
			</li>
          <li class="has-2dep "> <a href="#">소식</a>
            <ul class="gnb-2dep">
              <li> <a href="info_news.html"><span class="trans300"><em>공지사항</em></span></a> </li>
              <li> <a href="info_board.html"><span class="trans300"><em>자유게시판</em></span></a> </li>
              <li> <a href="info_faq.html"><span class="trans300"><em>FAQ</em></span></a> </li>
            </ul>
          </li>
          <li class="has-2dep"> <a href="#">장애관리</a>
            <ul class="gnb-2dep">
              <li> <a href="info_fault.html"><span class="trans300"><em>예방점검</em></span></a> </li>
            </ul>
          </li>
          <li class="has-2dep"> <a href="#">예방점검</a>
            <ul class="gnb-2dep">
               <li> <a href="info_preventive.html"><span class="trans300"><em>예방점검</em></span></a> </li>
            </ul>
          </li>
          <li class="on has-2dep"> <a href="#">교육자료관리</a>
            <ul class="gnb-2dep">
              <li> <a href="info_video.html"><span class="trans300"><em>동영상자료</em></span></a> </li>
              <li> <a href="info_file.html"><span class="trans300"><em>문서자료</em></span></a> </li>
            </ul>
          </li>
          <li  class="has-2dep"> <a href="#">관리자모드</a>
            <ul class="gnb-2dep">
              <li> <a href="info_admin_site.html"><span class="trans300"><em>사이트관리</em></span></a> </li>
              <li> <a href="info_admin_user.html"><span class="trans300"><em>사용자관리</em></span></a> </li>
              <li> <a href="info_admin_code.html"><span class="trans300"><em>코드관리</em></span></a> </li>
              <li> <a href="info_admin_problem.html"><span class="trans300"><em>장애통계</em></span></a> </li>
            </ul>
          </li>
        </ul>
      </nav>
    </div>
	  </div>
  </header>
  <!-- //header --> 
  <!-- container -->
  <div id="container" > 
    <!-- visual -->
    
    <section id="visual" style="background:#1e2e47 url(images/008.jpg) no-repeat 50% 0%">
      <div class="area visual-txt-con">
        <h2 class="visual-tit trans400"> 사용자관리 </h2>
        <p class="visual-sub-txt">COMPANY INTRODUCTION</p>
      </div>
    </section>
    <!-- //visual --> 
    <!-- middleArea -->
    <div id="middleArea"> 
      <!-- 서브메뉴1 ( 메뉴나열(PC) + 모바일메뉴  )-->
      
      <aside id="sideMenu">
        <div class="side-menu-inner area">
          <ul id="subGnb" class="snb clearfix">
            <div class="nav-on-icon main-move-line"><!-- 움직이는 바 --> 
              <span style="overflow: hidden; left: 724.719px; width: 171px; display: none;"></span> </div>
            <li class="on"> <a href="#"><span class="trans300"><em>전체</em></span></a> </li>
            <li> <a href="#"><span class="trans300"><em>사이트PM</em></span></a> </li>
            <li> <a href="#"><span class="trans300"><em>사용자</em></span></a> </li>
          </ul>
        </div>
      </aside>
      <aside id="sideMenuM" class="cm-top-menu clearfix">
        <div class="menu-location  location2"> <a href="javascript:;" class="cur-location"> <span>관리자모드</span> <i class="material-icons arrow"></i> </a>
          <ul class="location-menu-con">
            <li> <a href="#"><span class="trans300"><em>사이트관리</em></span></a> </li>
            <li> <a href="#"><span class="trans300"><em>사용자관리</em></span></a> </li>
            <li class="on"> <a href="#"><span class="trans300"><em>코드관리</em></span></a> </li>
            <li> <a href="#"><span class="trans300"><em>장애통계</em></span></a> </li>
          </ul>
        </div>
      </aside>
      <!-- // --> 
      
      <!-- 상단정보 (센터정렬) -->
      <aside id="contentInfoCon" class="content-info-style01 trans400">
        <h3 class="content-tit rubik">사이트 관리</h3>
      </aside>
      
      <!-- content -->
      <section id="content" class="area"> 
        
        <!----- 화면분할 시작 ----------->
        
        <div class="admin-cont" > 
          
          <!----- 게시판 리스트 시작 -----------> 
          <!--<div class="admin-title"><span><i class="fas fa-angle-right"></i> 사용자</span>  </div>--> 
          <!----- 조회 시작 ----------->
          <aticle >
            <div class="search-group " > 
              
              <!--관리자만--> 
              <a href="javascript:goDetail('등록');" id="123"> 등록 </a> </div>
          </aticle>
          <!----- 조회  끝 -----------> 
          
          <!----- 게시판 리스트 시작 ----------->
          
          <article class="bbs-list-con" >
            <h2 class="sound_only">관리자 게시판 목록</h2>
            <div class="bbs-list-Tbl" style="min-width: 1000px">
              <ul>
                <li class="bbs-list-TblTr bbs-list-TblTh">
                  <div class="mvInlineN">번호</div>
                  <div class="mvInlineN ">사이트명[별칭]</div>
                  <div class="mvInlineN ">발주처</div>
                  <div class="mvInlineN ">사업기간</div>
                  <div class="mvInlineN ">유지보수만료일</div>
                  <div class="mvInlineN ">담당자명</div>
                  <div class="mvInlineN ">연락처</div>
                </li>
                <li class="bbs-list-TblTr bbs-list-TblTd" >
                  <div class="mvInlineN td_num2"> 10 </div>
                  <div class="td_site"> 횡성군 상수관망 구축</div>
                  <div class="td_client">한국수자원공사</div>
                  <div class="mvInlinev td_datetime">2020-02-26 ~ 2020-12-25</div>
                  <div class="mvInlinev td_datetime">2020-02-26</div>
                  <div class="td_name"> 홍길동</div>
                  <div class="td_tel"> 010-987-9876</div>
                </li>
                <li class="bbs-list-TblTr bbs-list-TblTd">
                  <div class="mvInlineN td_num2"> 9 </div>
                  <div class="td_site"> 횡성군 상수관망 구축</div>
                  <div class="td_client">한국수자원공사</div>
                  <div class="mvInlinev td_datetime">2020-02-26 ~ 2020-12-25</div>
                  <div class="mvInlinev td_datetime">2020-02-26</div>
                  <div class="td_name"> 홍길동</div>
                  <div class="td_tel"> 010-987-9876</div>
                </li>
                <li class="bbs-list-TblTr bbs-list-TblTd">
                  <div class="mvInlineN td_num2"> 8 </div>
                  <div class="td_site"> 횡성군 상수관망 구축</div>
                  <div class="td_client">한국수자원공사</div>
                  <div class="mvInlinev td_datetime">2020-02-26 ~ 2020-12-25</div>
                  <div class="mvInlinev td_datetime">2020-02-26</div>
                  <div class="td_name"> 홍길동</div>
                  <div class="td_tel"> 010-987-9876</div>
                </li>
                <li class="bbs-list-TblTr bbs-list-TblTd">
                  <div class="mvInlineN td_num2"> 7 </div>
                  <div class="td_site"> 횡성군 상수관망 구축</div>
                  <div class="td_client">한국수자원공사</div>
                  <div class="mvInlinev td_datetime">2020-02-26 ~ 2020-12-25</div>
                  <div class="mvInlinev td_datetime">2020-02-26</div>
                  <div class="td_name"> 홍길동</div>
                  <div class="td_tel"> 010-987-9876</div>
                </li>
                <li class="bbs-list-TblTr bbs-list-TblTd">
                  <div class="mvInlineN td_num2"> 6 </div>
                  <div class="td_site"> 횡성군 상수관망 구축</div>
                  <div class="td_client">한국수자원공사</div>
                  <div class="mvInlinev td_datetime">2020-02-26 ~ 2020-12-25</div>
                  <div class="mvInlinev td_datetime">2020-02-26</div>
                  <div class="td_name"> 홍길동</div>
                  <div class="td_tel"> 010-987-9876</div>
                </li>
				  <li class="bbs-list-TblTr bbs-list-TblTd">
                  <div class="mvInlineN td_num2"> 5 </div>
                  <div class="td_site"> 횡성군 상수관망 구축</div>
                  <div class="td_client">한국수자원공사</div>
                  <div class="mvInlinev td_datetime">2020-02-26 ~ 2020-12-25</div>
                  <div class="mvInlinev td_datetime">2020-02-26</div>
                  <div class="td_name"> 홍길동</div>
                  <div class="td_tel"> 010-987-9876</div>
                </li>
				  <li class="bbs-list-TblTr bbs-list-TblTd">
                  <div class="mvInlineN td_num2"> 4 </div>
                  <div class="td_site"> 횡성군 상수관망 구축</div>
                  <div class="td_client">한국수자원공사</div>
                  <div class="mvInlinev td_datetime">2020-02-26 ~ 2020-12-25</div>
                  <div class="mvInlinev td_datetime">2020-02-26</div>
                  <div class="td_name"> 홍길동</div>
                  <div class="td_tel"> 010-987-9876</div>
                </li>
				  <li class="bbs-list-TblTr bbs-list-TblTd">
                  <div class="mvInlineN td_num2"> 3 </div>
                  <div class="td_site"> 횡성군 상수관망 구축</div>
                  <div class="td_client">한국수자원공사</div>
                  <div class="mvInlinev td_datetime">2020-02-26 ~ 2020-12-25</div>
                  <div class="mvInlinev td_datetime">2020-02-26</div>
                  <div class="td_name"> 홍길동</div>
                  <div class="td_tel"> 010-987-9876</div>
                </li>
				  <li class="bbs-list-TblTr bbs-list-TblTd">
                  <div class="mvInlineN td_num2"> 2 </div>
                  <div class="td_site"> 횡성군 상수관망 구축</div>
                  <div class="td_client">한국수자원공사</div>
                  <div class="mvInlinev td_datetime">2020-02-26 ~ 2020-12-25</div>
                  <div class="mvInlinev td_datetime">2020-02-26</div>
                  <div class="td_name"> 홍길동</div>
                  <div class="td_tel"> 010-987-9876</div>
                </li>
				  <li class="bbs-list-TblTr bbs-list-TblTd">
                  <div class="mvInlineN td_num2"> 1 </div>
                  <div class="td_site"> 횡성군 상수관망 구축</div>
                  <div class="td_client">한국수자원공사</div>
                  <div class="mvInlinev td_datetime">2020-02-26 ~ 2020-12-25</div>
                  <div class="mvInlinev td_datetime">2020-02-26</div>
                  <div class="td_name"> 홍길동</div>
                  <div class="td_tel"> 010-987-9876</div>
                </li>
              </ul>
            </div>
          </article>
          <!--  게시판 리스트 종료 --> 
         <div class="paging"> <a href="#" class="cur">1</a> <a href="#">2</a> <a href="#">3</a> </div>  
        </div>
        <!-----  화면분할  끝 -----------> 
        
        <!-- //컨텐츠 내용 --> 
      </section>
      <!-- //content --> 
    </div>
    <!-- //middleArea --> 
  </div>
  <!-- //container --> 
  <!-- footer -->
  
  <footer id="footer" class="sub"> <a href="#wrap" class="to-top-btn"><i class="material-icons"></i></a>
    <div id="footerInner" class="clearfix">
      <article id="footerTop" class="">
        <div class="area-box clearfix">
          <div class="footer-info-cont"> <span class="copyright">© 2020 <em>Greentech INC</em> All rights reserved.</span> </div>
        </div>
      </article>
    </div>
  </footer>
  
  <!-- 모달 레이어팝업 -->
  <article class="modal-fixed-pop-wrapper">
    <div class="modal-fixed-pop-inner">
      <div class="modal-loading"><span class="loading"></span></div>
      <div class="modal-inner-box">
        <div class="modal-inner-content"> 
          <!-- ajax 내용 --> 
        </div>
      </div>
    </div>
  </article>
  
  <!-- //footer --> 
</div>
<!-- //code --> 
	<!--sitemap Popup Start -->

	
	<article class="layerpop-cont">
	<div id="mask"></div>    <div id="layerbox-sitemap" class="layerpopsitemap" style="width: 900px; height: 360px;">
        <article class="layerpop_area">
        <div class="title">사이트맵</div>
		
        <a href="javascript:popupClosesitemap();" class="layerpop_close" id="layerbox_close"><i class="material-icons"></i></a> 
        <div class="content">
        <!-- 문의 폼 시작 -->
        
         
          <article class="sitemap-wrapper">
		
		<ul >
         <li>
				<h2>소식</h2>
           <ul class="sitemap-2dep">
              <li> <a href="info_news.html"><span class="trans300"><em>공지사항</em></span></a> </li>
              <li> <a href="info_board.html"><span class="trans300"><em>자유게시판</em></span></a> </li>
              <li> <a href="info_faq.html"><span class="trans300"><em>FAQ</em></span></a> </li>
            </ul>
          </li>
          <li>
				<h2>장애관리</h2>
            <ul class="sitemap-2dep">
              <li> <a href="info_fault.html"><span class="trans300"><em>장애관리</em></span></a> </li>
            </ul>
          </li>
          <li>
				<h2>예방점검</h2>
           <ul class="sitemap-2dep">
              <li>  <a href="info_preventive.html"><span class="trans300"><em>예방점검</em></span></a>  </li>
            </ul>
          </li>
          <li>
				<h2>교육자료관리</h2>
            <ul class="sitemap-2dep">
                <li> <a href="info_video.html"><span class="trans300"><em>동영상자료</em></span></a> </li>
              <li> <a href="info_file.html"><span class="trans300"><em>문서자료</em></span></a> </li>
            </ul>
          </li>
			
			<li> <h2>관리자모드</h2>
            <ul class="sitemap-2dep">
              <li> <a href="info_admin_site.html"><span class="trans300"><em>사이트관리</em></span></a> </li>
              <li> <a href="info_admin_user.html"><span class="trans300"><em>사용자관리</em></span></a> </li>
              <li> <a href="info_admin_code.html"><span class="trans300"><em>코드관리</em></span></a> </li>
              <li> <a href="info_admin_problem.html"><span class="trans300"><em>장애통계</em></span></a> </li>
            </ul>
          </li>
          
        </ul>
		
		
	</article>
       
       
		  
    
        </div>
        </article>
    </div>
	</article>
    

	
	
	
    <!--sitemap Popup End -->
	
<!--code  Popup Start -->
<article class="layerpop-cont">
  <div id="mask"></div>
  <div id="layerbox" class="layerpop" style="width: 600px; height: 830px;">
    <article class="layerpop_area">
      <div class="title">사이트 관리</div>
      <a href="javascript:popupClose();" class="layerpop_close" id="layerbox_close"><i class="material-icons"></i></a>
      <div class="content"> 
        <!-- 문의 폼 시작 -->
        
        <article class="layerpop-write-tbl-box">
          <table  class="layerpop-write-tbl"  >
            <tbody>
              <tr>
                <th scope="row">* 사이트명</th>
                <td><input type="text" class="write-input width100" name="name" placeholder="사이트명을 입력해주세요"></td>
              </tr>
              <tr>
                <th scope="row">* 별칭  <span class="info_text" style="padding-left: 10px"> <i class="fas fa-info-circle"></i> 사이트 줄임말</span></th>
                <td><input type="text"  class="write-input width100" name="name" placeholder="별칭을 입력해주세요">
                 </td>
              </tr>
              <tr>
                <th scope="row"> 사업기간</th>
                <td><input type="date" class="write-input width40" name="name" placeholder="기간을 선택해주세요">
                  ~
                  <input type="date" class="write-input width40" name="name" placeholder="기간을 선택해주세요"></td>
              </tr>
              <tr>
                <th scope="row"> 유지보수만료일</th>
                <td><input type="date" class="write-input width40" name="name" placeholder="날짜을 선택해주세요"></td>
              </tr>
              <tr>
                <th scope="row">* 발주처</th>
                <td><input type="text" class="write-input width100" name="name" placeholder="발주처를 입력해주세요"></td>
              </tr>
              <tr>
                <th scope="row">* 담당자</th>
                <td><input type="text" class="write-input width100" name="name" placeholder="담당자를 입력해주세요"></td>
              </tr>
              <tr>
                <th scope="row">* 전화번호</th>
                <td><input type="text" class="write-input width100" name="name" placeholder="전화번호를 입력해주세요"></td>
              </tr>
              <tr>
                <th scope="row">* 현장대리인</th>
                <td><input type="text" class="write-input width100" name="name" placeholder="현장대리인을 입력해주세요"></td>
              </tr>
            </tbody>
          </table>
        </article>
        
        <!-- // 문의 폼 끝 -->
        <div class="btn-controls" style="float: left; display: block; width: 100%; " >
          <button type="submit" class="btn-style01">삭제</button>
          <button type="submit" class="btn-style01">저장</button>
          <button type="submit" class="btn-style01"  onClick="javascript:popupClose();">닫기</button>
        </div>
      </div>
    </article>
  </div>
</article>
<!--Popup End -->

</body>
</html>