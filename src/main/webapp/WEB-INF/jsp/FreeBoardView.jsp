<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>

<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>사후관리시스템</title>
<meta name="Subject" content="news">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 모바일사이트, 반응형사이트 제작시 사용 -->

<link rel="stylesheet" href="css/reset.css">
<link rel="stylesheet" href="css/common.css">

<link rel="stylesheet" href="css/layout.css">

<link rel="stylesheet" href="css/layout_responsive.css">

	
<link rel="stylesheet" href="css/board.css">
<!-- 게시판 제작시 사용 -->
<link rel="stylesheet" href="css/board_responsive.css">
<!-- 게시판(반응형, 모바일) 제작시 사용 --> 

<!-- 아이콘폰트 -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="css/fontawesome-free-5.14.0-web/css/all.css" rel="stylesheet">
	
 <!-- 용도모르는 스크립트 확인요망!!! -->	
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> 
<script>window.jQuery || document.write('<script src="http://code.jquery.com/jquery-1.8.3.min.js"><\/script>')</script>



<script src="js/common.js"></script> 
<script src="js/layer_popup.js"></script> 
<script src="js/jquery.validate.js"></script> 

<link rel="stylesheet" type="text/css" href="css/slick.css">

<script src="js/slick.js"></script> 

<!-- 스크롤 -->
<link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script> 
<script type="text/javascript" src="js/nav.js"></script> 

<script>
	$(function  () {
		dep1 = 04,
		dep2 = 01;
		if(dep1 > 0){
			$(".content-sub-tit").css("display", "none");
			$("#footer").addClass("sub");
		};
	})
</script>
	
	
<!-- 레이어팝업 스크립트 --> <link rel="stylesheet" href="css/layerpop.css">
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script> 
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script> 
<script src="js/layerpop.js"></script>
</head>


<body>

<!-- code -->
<div id="wrap"> 
  <!-- header -->
  <header id="header" class="fixed-header clearfix">
    <div class="gnb-overlay-bg"></div>
    <div id="gnbWrap" class="trans300">
      <div id="HeaderInner" class="clearfix">
        <h1 class="logo trans300"> <a href="#"> <img src="images/logo.png" width="185" height="41" alt="사후관리시스템"/> </a> </h1>
        <div class="header-util-box trans300 clearfix"> 
           <div class="header-lang trans300"> <span> <a  href="#">logout</a> </span> </div>
          <!-- 사이트맵 버튼 ( 기본 라인 三 ) -->
         <button onclick="javascript:goDetailsitemap('사이트맵');" class="sitemap-line-btn sitemap-open-btn" title="사이트맵 열기"> <span class="line line1"></span> <span class="line line2"></span> <span class="line line3"></span> </button>
          <!-- GNB Mobile --> 
          <a href="javascript;" class="nav-open-btn" title="네비게이션 열기"> <span class="line line1"></span> <span class="line line2"></span> <span class="line line3" ></span> </a> </div>
      </div>
      <nav id="gnb" class="each-menu">
		 
        <ul class="clearfix">
          <li class="dep-1  active on"> <a href="#">소식</a>
            <ul class="gnb-2dep" >
              <li> <a href="info_news.html"><span class="trans300"><em>공지사항</em></span></a> </li>
              <li> <a href="info_board.html"><span class="trans300"><em>자유게시판</em></span></a> </li>
              <li> <a href="info_faq.html"><span class="trans300"><em>FAQ</em></span></a> </li>
            </ul>
          </li>
          <li class="dep-1"> <a href="#">장애관리</a>
            <ul class="gnb-2dep" style="display: none;">
              <li> <a href="info_fault.html"><span class="trans300"><em>장애관리</em></span></a> </li>
            </ul>
          </li>
          <li class="dep-1"> <a href="#">예방점검</a>
            <ul class="gnb-2dep" style="display: none;">
              <li> <a href="info_preventive.html"><span class="trans300"><em>예방점검</em></span></a> </li>
            </ul>
          </li>
          <li class="dep-1"> <a href="#">교육자료관리</a>
            <ul class="gnb-2dep" style="display: none;">
              <li> <a href="info_video.html"><span class="trans300"><em>동영상자료</em></span></a> </li>
              <li> <a href="info_file.html"><span class="trans300"><em>문서자료</em></span></a> </li>
            </ul>
          </li>
          
        </ul>
      </nav>
      <div class="gnb-overlay-bg-m"></div>
      <nav id="gnbM" class="trans400" >
        <ul id="navigation">
          <li class="has-2dep ">  
			 <div class="header-lang-m " >홍길동님 환영합니다.  <span > <a  href="#">logout</a> </span>  </div>
			</li>
          <li class="has-2dep "> <a href="#">소식</a>
            <ul class="gnb-2dep">
              <li> <a href="info_news.html"><span class="trans300"><em>공지사항</em></span></a> </li>
              <li> <a href="info_board.html"><span class="trans300"><em>자유게시판</em></span></a> </li>
              <li> <a href="info_faq.html"><span class="trans300"><em>FAQ</em></span></a> </li>
            </ul>
          </li>
          <li class="has-2dep"> <a href="#">장애관리</a>
            <ul class="gnb-2dep">
              <li> <a href="info_fault.html"><span class="trans300"><em>장애관리</em></span></a> </li>
            </ul>
          </li>
          <li class="has-2dep"> <a href="#">예방점검</a>
            <ul class="gnb-2dep">
               <li> <a href="info_preventive.html"><span class="trans300"><em>예방점검</em></span></a> </li>
            </ul>
          </li>
          <li class="on has-2dep"> <a href="#">교육자료관리</a>
            <ul class="gnb-2dep">
              <li> <a href="info_video.html"><span class="trans300"><em>동영상자료</em></span></a> </li>
              <li> <a href="info_file.html"><span class="trans300"><em>문서자료</em></span></a> </li>
            </ul>
          </li>
         
        </ul>
      </nav>
    </div>
  </header>
   
  <!-- //header --> 
<!-- container -->

<div id="container">
<!-- visual -->

<section id="visual" style="background:#1e2e47 url(images/008.jpg) no-repeat 50% 0%">
  <div class="area visual-txt-con">
    <h2 class="visual-tit trans400"> 소식 </h2>
    <p class="visual-sub-txt">COMPANY INTRODUCTION</p>
  </div>
</section>
<!-- //visual --> 
<!-- middleArea -->
<div id="middleArea">
<!-- 서브메뉴1 ( 메뉴나열(PC) + 모바일메뉴  )-->

<aside id="sideMenu">
  <div class="side-menu-inner area">
    <ul id="subGnb" class="snb clearfix">
      <div class="nav-on-icon main-move-line"><!-- 움직이는 바 --> 
        <span style="overflow: hidden; left: 724.719px; width: 171px; display: none;"></span> </div>
      <li > <a href="info_news.html"><span class="trans300"><em>공지사항</em></span></a> </li>
      <li class="on"> <a href="info_board.html"><span class="trans300"><em>자유게시판</em></span></a> </li>
      <li> <a href="info_faq.html"><span class="trans300"><em>FAQ</em></span></a> </li>
    </ul>
  </div>
</aside>
<aside id="sideMenuM" class="cm-top-menu clearfix">
  <div class="menu-location  location2"> <a href="javascript:;" class="cur-location"> <span>자유게시판</span> <i class="material-icons arrow"></i> </a>
    <ul class="location-menu-con">
      <li> <a href="info_news.html"><span class="trans300"><em>공지사항</em></span></a> </li>
      <li class="on"> <a href="info_board.html"><span class="trans300"><em>자유게시판</em></span></a> </li>
      <li> <a href="info_faq.html"><span class="trans300"><em>FAQ</em></span></a> </li>
    </ul>
  </div>
</aside>
<!-- // --> 

<!-- 상단정보 (정렬) -->
<aside id="contentInfoCon" class="content-info-style01 trans400">
  <h3 class="content-tit rubik">자유게시판</h3>
</aside>

<!-- content -->
<section id="content" class="area">

<!-- 컨텐츠 내용 --> 

<!-- 게시판읽기  시작 -->

<div id="bbs-view"  class="bbs-view-con">
<article class="bbs-view-title" >
<header>
  <h2 id="bo_v_title" class="bbs-view-title"> <span class="bo_v_tit"> 게시판제목입니다.</span> </h2>
</header>
<div  class="bbs-view-info">
  <h2>페이지 정보</h2>
  <span class="sound_only">작성자</span><i class="far fa-user"></i> <strong><span class="sv_member">관리자</span></strong> <span class="sound_only"> 댓글 </span><i class="far fa-comment-dots"></i> <strong><a href="#">3건</a></strong> <span class="sound_only"> 조회 </span><i class="far fa-eye"></i> <strong> 3,971회</strong> <span class="sound_only">작성일</span><i class="far fa-clock" ></i> <strong>20-02-06 15:21</strong> </div>
<div class="bo_v_atc">
  <h2 class="bo_v_atc_title">본문</h2>
  
  <!-- 본문 내용 시작 { -->
  <div class="bo_v_con" >
    <p> 그린텍아이엔씨 등 12개 환경기업이 '2020년 우수환경산업체'로 지정됐다. </p>
    <br>
    <p> 25일 한국환경산업기술원에 따르면 2012년부터 시작한 우수환경산업체 지정제도는 사업실적 및 기술력이 뛰어나 해외환경시장에서 성장잠재력이 큰 기업을 지정해 ‘대한민국 환경분야 국가대표’ 기업으로 육성하는 것을 말한다.</p>
    <br>
    <p> 제도시행이후 지금까지 101개사가 우수환경산업체로 지정됐으며 올해 새롭게 지정된 기업들을 포함해 남아 있는 우수환경산업체는 53개사다. </p>
    <br>
    <p> 우수환경산업체로 지정된 기업은 환경부와 한국환경산업기술원으로부터 홍보활동, 금융 및 수출역량 강화 등 기업경쟁력 강화를 위한 지원을 받게 된다. </p>
    <br>
    <p> 특히 올해 지정된 기업은 판로개척을 위한 기술·제품 모형 및 기업 홍보영상 제작, 해외진출 행사 참가비용 등을 지원받는다. </p>
    <br>
    <p> 이밖에 한국환경산업기술원에서 수행하는 환경기술 개발, 정책자금 융자, 해외진출 지원 사업 등을 신청하면 선정 심사과정에서 가점을 받을 수 있다. </p>
    <br>
    <p> 유제철 한국환경산업기술원장은 “우수환경산업체 지정제도를 통해 우수한 환경기업이 기업가치 1조원 이상의 유니콘기업으로 성장하도록 지원을 강화해 그린뉴딜 성공의 본보기로 안착하도록 지원하겠다”라고 밝혔다.</p>
  </div>
  <!-- } 본문 내용 끝 --> 
  	<div id="bo_v_file" class="bo_v_file_cnt" >
		<ul>
			<li><a href="info_news.html"> <i class="far fa-file"></i> 사용자 매뉴얼 2.3 MB</a></li>
		
		</ul>
</div>
</div>
<div class="bo_v_share"> 
  <!-- 댓글 시작 { -->
  <div class="bbs-view-comment">
    <textarea class="bbs-view-comment" ></textarea>
    <div class="com_file_input">
      <input type="text" readonly="readonly" title="File Route" id="file_route">
      <label> 파일불러오기
        <input type="file" onchange="javascript:document.getElementById('file_route').value=this.value">
      </label>
      <button class="cm-fr btn-style01"><i class="far fa-comments"></i> 답글달기</button>
    </div>
  </div>
  <section id="bo_vc" >
    <h2>댓글목록</h2>
    <article >
      <header>
        <h2>웹사이팅님의  댓글</h2>
        <span class="member"><i class="far fa-user"></i> 웹사이팅</span> <span class="sound_only">작성일</span><span class="bo_vc_hdinfo"><i class="fa fa-clock" aria-hidden="true"></i>
        <time datetime="2020-02-07T15:45:00+09:00">18-02-07 15:45</time>
        </span> <a href="#" class="cm-fr  ml10"  title="삭제"> <i class="fas fa-times"></i><span class="sound_only" >삭제</span></a><!-- 삭제 --> 
        <a href="#" class="cm-fr" title="수정"> <i class="fas fa-edit" ></i><span class="sound_only" >수정</span></a><!-- 수정 --> 
        
      </header>
      
      <!-- 댓글 출력 -->
      <div class="cmt_contents">
        <p> 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 </p>
      </div>
    </article>
  </section>
  <section id="bo_vc" >
    <h2>댓글목록</h2>
    <article >
      <header>
        <h2>웹사이팅님의  댓글</h2>
        <span class="member"><i class="far fa-user"></i> 웹사이팅</span> <span class="sound_only">작성일</span><span class="bo_vc_hdinfo"><i class="fa fa-clock" aria-hidden="true"></i>
        <time datetime="2020-02-07T15:45:00+09:00">18-02-07 15:45</time>
        </span> <a href="#" class="cm-fr  ml10"  title="삭제"> <i class="fas fa-times"></i><span class="sound_only" >삭제</span></a><!-- 삭제 --> 
        <a href="#" class="cm-fr" title="수정"> <i class="fas fa-edit" ></i><span class="sound_only" >수정</span></a><!-- 수정 --> 
        
      </header>
      
      <!-- 댓글 출력 -->
      <div class="cmt_contents">
        <p> 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 댓글입니다 </p>
      </div>
    </article>
  </section>
  <!--  댓글 끝 -->
  
  <div class="cm-btn-controls">
    <button type="submit" class="btn-style01" onClick="location.href='info_board_write.html'"> 수정 </button>
    <button type="submit" class="btn-style01"> 삭제 </button>
    <button type="submit" class="btn-style01" onClick="location.href='info_board.html'"> 목록 </button>
  </div>
 
</div>

<!-- // 게시판읽  끝 --> 
<!-- //컨텐츠 내용 -->
</article>

<!-- //content -->
</div>
</section>	
<!-- //middleArea -->
</div>

<!-- //container --> 
<!-- footer -->

<footer id="footer" class="sub"> 
  <a href="#wrap" class="to-top-btn"><i class="material-icons"></i></a>
  <div id="footerInner" class="clearfix">
    <article id="footerTop" class="">
      <div class="area-box clearfix">
        <div class="footer-info-cont"> <span class="copyright">© 2020 <em>Greentech INC</em> All rights reserved.</span> </div>
      </div>
    </article>
  </div>
</footer>

<!-- 모달 레이어팝업 -->
<article class="modal-fixed-pop-wrapper">
  <div class="modal-fixed-pop-inner">
    <div class="modal-loading"><span class="loading"></span></div>
    <div class="modal-inner-box">
      <div class="modal-inner-content"> 
        <!-- ajax 내용 --> 
      </div>
    </div>
  </div>
</article>
<!-- //footer -->
</div>
</div>	
	
<!-- //code -->
	<!--sitemap Popup Start -->

	
	<article class="layerpop-cont">
	<div id="mask"></div>		  
    <div id="layerbox-sitemap" class="layerpopsitemap" style="width: 900px; height: 330px;">
        <article class="layerpop_area">
        <div class="title">사이트맵</div>
		
        <a href="javascript:popupClosesitemap();" class="layerpop_close" id="layerbox_close"><i class="material-icons"></i></a> 
        <div class="content">
       
        
         
          <article class="sitemap-wrapper">
		
		<ul >
         <li>
				<h2>소식</h2>
           <ul class="sitemap-2dep">
              <li> <a href="info_news.html"><span class="trans300"><em>공지사항</em></span></a> </li>
              <li> <a href="info_board.html"><span class="trans300"><em>자유게시판</em></span></a> </li>
              <li> <a href="info_faq.html"><span class="trans300"><em>FAQ</em></span></a> </li>
            </ul>
          </li>
          <li>
				<h2>장애관리</h2>
            <ul class="sitemap-2dep">
              <li> <a href="info_fault.html"><span class="trans300"><em>장애관리</em></span></a> </li>
            </ul>
          </li>
          <li>
				<h2>예방점검</h2>
           <ul class="sitemap-2dep">
              <li>  <a href="info_preventive.html"><span class="trans300"><em>예방점검</em></span></a>  </li>
            </ul>
          </li>
          <li>
				<h2>교육자료관리</h2>
            <ul class="sitemap-2dep">
                <li> <a href="info_video.html"><span class="trans300"><em>동영상자료</em></span></a> </li>
              <li> <a href="info_file.html"><span class="trans300"><em>문서자료</em></span></a> </li>
            </ul>
          </li>
			

          
        </ul>
		
		
	</article>
       
       
		  
    
        </div>
        </article>
    </div>
	</article>

    <!--sitemap Popup End -->

</body>
</html>