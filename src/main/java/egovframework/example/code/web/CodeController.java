package egovframework.example.code.web;


import java.util.HashMap;
import java.util.List;

import egovframework.example.code.service.CodeService;
import egovframework.example.sample.service.EgovSampleService;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springmodules.validation.commons.DefaultBeanValidator;

@Controller
public class CodeController {

	/** codeService */
	@Resource(name = "codeService")
	private CodeService codeService;
	
	@RequestMapping("/getCode.do")
	protected ModelAndView getCode() throws Exception
	{
		HashMap map = new HashMap();
		map.put("MST_CD", "101");
		
		List codeList = codeService.detailCodeselect(map);		
		
		ModelAndView mav = new ModelAndView("jsonView");
		
		mav.addObject("codeList", codeList);
		
		return mav;
	}

}
