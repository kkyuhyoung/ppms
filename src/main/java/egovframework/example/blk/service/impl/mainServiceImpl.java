package egovframework.example.blk.service.impl;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import egovframework.example.blk.service.mainService;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;

@Service("mainService")
public class mainServiceImpl extends EgovAbstractServiceImpl implements mainService{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(mainServiceImpl.class);

	//선언부 해당 DAO 사용시 선언
	@Resource(name = "mainDAO")
	private mainDAO mainDAO;

	
	public List SelectLogUser(HashMap map) throws Exception {
		return mainDAO.SelectLogUser(map);
	}

	public List Select_BBS_MST_MNG(HashMap map) throws Exception {
		return mainDAO.Select_BBS_MST_MNG(map);
	}
	
	public List Select_BBS_MST_MNG_View(HashMap map) throws Exception {
		return mainDAO.Select_BBS_MST_MNG_View(map);
	}
	
	public int Update_BBS_MST_CNT(HashMap map) throws Exception {
		return mainDAO.Update_BBS_MST_CNT(map);
	}
	
	public List Select_CD_INFO(HashMap map) throws Exception {
		return mainDAO.Select_CD_INFO(map);
	}
	
	public int Merge_BBS_MST_MNG(HashMap map) throws Exception {
		return mainDAO.Merge_BBS_MST_MNG(map);
	}
	
	public int Delete_BBS_MST_MNG(HashMap map) throws Exception {
		return mainDAO.Delete_BBS_MST_MNG(map);
	}
	
}
