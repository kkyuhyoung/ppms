package egovframework.example.blk.service.impl;

import java.util.HashMap;
import java.util.List;

import egovframework.example.blk.service.blkService;
import egovframework.example.code.service.CodeService;
import egovframework.example.code.service.impl.CodeDAO;
import egovframework.example.code.service.impl.CodeServiceImpl;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service("blkService")
public class blkServiceImpl extends EgovAbstractServiceImpl implements blkService {

	private static final Logger LOGGER = LoggerFactory.getLogger(blkServiceImpl.class);

	//선언부 해당 DAO 사용시 선언
	@Resource(name = "blkDAO")
	private blkDAO blkDAO;

	
	@Override
	public List blktreeselect(HashMap map) throws Exception {
		return blkDAO.blktreeselect(map);
	}
	
	public List mesrselect(HashMap map) throws Exception {
		return blkDAO.mesrselect(map);
	}
	
	public List mesrselect2(HashMap map) throws Exception {
		return blkDAO.mesrselect2(map);
	}
	
	public List boardSearch(HashMap map) throws Exception{
		return blkDAO.boardSearch(map);
	}
}