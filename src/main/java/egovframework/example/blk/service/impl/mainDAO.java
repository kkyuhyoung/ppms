package egovframework.example.blk.service.impl;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;

@Repository("mainDAO")
public class mainDAO extends EgovAbstractDAO{

	@SuppressWarnings("rawtypes")
	public List SelectLogUser(HashMap map) throws Exception {
		return (List) list("mainDAO.SelectLogUser", map);
	}
	
	@SuppressWarnings("rawtypes")
	public List Select_BBS_MST_MNG(HashMap map) throws Exception {
		return (List) list("mainDAO.Select_BBS_MST_MNG", map);
	}
	
	@SuppressWarnings("rawtypes")
	public List Select_BBS_MST_MNG_View(HashMap map) throws Exception {
		return (List) list("mainDAO.Select_BBS_MST_MNG_View", map);
	}
	
	@SuppressWarnings("rawtypes")
	public int Update_BBS_MST_CNT(HashMap map) throws Exception {
		return (int)update("mainDAO.Update_BBS_MST_CNT", map);
	}
	
	@SuppressWarnings("rawtypes")
	public List Select_CD_INFO(HashMap map) throws Exception {
		return (List) list("mainDAO.Select_CD_INFO", map);
	}
	
	@SuppressWarnings("rawtypes")
	public int Merge_BBS_MST_MNG(HashMap map) throws Exception {
		return (int)insert("mainDAO.Merge_BBS_MST_MNG", map);
	}
	
	@SuppressWarnings("rawtypes")
	public int Delete_BBS_MST_MNG(HashMap map) throws Exception {
		return (int)update("mainDAO.Delete_BBS_MST_MNG", map);
	}
	
	
	
	
}
