package egovframework.example.blk.service.impl;

import java.util.HashMap;
/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import java.util.List;
import egovframework.rte.psl.dataaccess.EgovAbstractDAO;

import org.springframework.stereotype.Repository;

@Repository("blkDAO")
public class blkDAO extends EgovAbstractDAO {

	/**
	 * 블록의 목록을 가져옴.
	 * @return 블록 목록
	 * @exception Exception
	 */
	@SuppressWarnings("rawtypes")
	public List blktreeselect(HashMap map) throws Exception {
		return (List) list("blkDAO.blktreeselect", map);
	}
	
	@SuppressWarnings("rawtypes")
	public List mesrselect(HashMap map) throws Exception {
		return (List) list("blkDAO.mesrselect", map);
	}
	
	@SuppressWarnings("rawtypes")
	public List mesrselect2(HashMap map) throws Exception {
		return (List) list("blkDAO.mesrselect2", map);
	}
	
	@SuppressWarnings("rawtypes")
	public List boardSearch(HashMap map) throws	Exception{
		return (List) list("blkDAO.board_Search", map);
	}
}

