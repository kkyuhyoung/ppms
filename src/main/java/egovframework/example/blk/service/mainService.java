package egovframework.example.blk.service;

import java.util.HashMap;
import java.util.List;

public interface mainService {

	List SelectLogUser(HashMap map) throws Exception;
	
	List Select_BBS_MST_MNG(HashMap map) throws Exception;
	
	List Select_BBS_MST_MNG_View(HashMap map) throws Exception;
	
	int Update_BBS_MST_CNT(HashMap map) throws Exception;
	
	List Select_CD_INFO(HashMap map) throws Exception;
	
	int Merge_BBS_MST_MNG(HashMap map) throws Exception;
	
	int Delete_BBS_MST_MNG(HashMap map) throws Exception;
	
}
