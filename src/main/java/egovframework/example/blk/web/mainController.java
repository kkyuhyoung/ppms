package egovframework.example.blk.web;

import java.util.HashMap;
import java.util.List;

import egovframework.example.blk.service.mainService;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import javax.annotation.Resource;
import javax.crypto.Mac;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springmodules.validation.commons.DefaultBeanValidator;

@Controller
public class mainController {

	/** mainService */
	@Resource(name = "mainService")
	private mainService mainService;

	//로그인
	@RequestMapping("/Login.do")
	protected ModelAndView Login(@RequestParam("USER_ID") String USER_ID, @RequestParam("USER_PWD") String USER_PWD,
			@RequestParam("GBN") String GBN, HttpServletRequest request) throws Exception {
		HashMap map = new HashMap();
		map.put("USER_ID", USER_ID);
		map.put("USER_PWD", USER_PWD);
		map.put("GBN", GBN);

		System.out.println("리퀘스트 : " + request.toString());
		
		List userList = mainService.SelectLogUser(map);

		ModelAndView mav = new ModelAndView("jsonView");

		if (userList.size() == 1) {
			request.getSession().setAttribute("USER_ID", USER_ID);
			request.getSession().setAttribute("USER_NM", ((HashMap<String,Object>)userList.get(0)).get("USER_NM").toString());
			request.getSession().setAttribute("AUTH_SETUP", ((HashMap<String,Object>)userList.get(0)).get("AUTH_SETUP").toString());
			
			mav.addObject("data", "actionBoard.do?MNU_CD=0001&VIEW_NM=noticeBoard");
		}else {
			//로그인 실패 처리
		}
		return mav;
	}
	
	//로그아웃
	@RequestMapping("/Logout.do")
	protected ModelAndView actionLogout(HttpServletRequest request) throws Exception {
		
		System.out.println("리퀘스트  : " + request.toString());
		System.out.println("세션-USER_ID : " + request.getSession().getAttribute("USER_ID"));
		System.out.println("세션-USER_NM : " + request.getSession().getAttribute("USER_NM"));
		request.getSession().removeAttribute("USER_ID");
		request.getSession().removeAttribute("USER_NM");
		
		System.out.println("세션-USER_ID : " + request.getSession().getAttribute("USER_ID"));
		System.out.println("세션-USER_NM : " + request.getSession().getAttribute("USER_NM"));
		
		ModelAndView mav = new ModelAndView("jsonView");
		mav.setViewName("login");

		return mav;
	}

	@RequestMapping("/SearchUserInfo.do")
	protected ModelAndView SearchUserInfo(@RequestParam("TEL") String TEL, @RequestParam("GBN") String GBN)
			throws Exception {
		HashMap map = new HashMap();
		map.put("TEL", TEL);
		map.put("GBN", GBN);

		List userList = mainService.SelectLogUser(map);

		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("data", userList);

		return mav;
	}

	@RequestMapping("/actionMain.do")
	protected ModelAndView actionMain() throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		mav.setViewName("login");

		return mav;
	}
	
	@RequestMapping("/actionBoard.do")
	protected ModelAndView actionBoard(@RequestParam("MNU_CD") String MNU_CD, 
									   @RequestParam("VIEW_NM") String VIEW_NM, 
									   @RequestParam(value="PAGE_NO", required=false, defaultValue="1") String PAGE_NO,
									   @RequestParam(value="CONDITION", required=false, defaultValue="0") String CONDITION,
									   @RequestParam(value="SEARCH_WORD", required=false, defaultValue="") String SEARCH_WORD,
									   HttpServletRequest request) throws Exception {
		
		System.out.println("리퀘스트  : " + request.toString());
		System.out.println("세션-USER_ID : " + request.getSession().getAttribute("USER_ID"));
		System.out.println("세션-USER_NM : " + request.getSession().getAttribute("USER_NM"));
		System.out.println("세션-AUTH_SETUP : " + request.getSession().getAttribute("AUTH_SETUP"));
		
		//로그인 사용자 정보
		HashMap<String,Object> loginfo = new HashMap<>();
		loginfo.put("USER_ID", request.getSession().getAttribute("USER_ID").toString());
		loginfo.put("USER_NM", request.getSession().getAttribute("USER_NM").toString());
		loginfo.put("AUTH_SETUP", request.getSession().getAttribute("AUTH_SETUP").toString());
		
		//페이징
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(Integer.parseInt(PAGE_NO)); // 현재 페이지 번호
		paginationInfo.setRecordCountPerPage(15); // 한 페이지당 게시되는 게시물 건 수
		paginationInfo.setPageSize(10); // 페이지 리스트에 게시되는 페이지 건 수

		HashMap map = new HashMap();
		map.put("MNU_CD", MNU_CD);
		map.put("ITEM_1_CD", "0010");
		map.put("ITEM_2_CD", "");
		if(SEARCH_WORD.trim().equals("")) {
			map.put("CONDITION", "*");
		}else{
			map.put("CONDITION", CONDITION);
		}
		map.put("SEARCH_WORD", SEARCH_WORD.trim());

		int intStart = paginationInfo.getFirstRecordIndex();
		int intEnd = intStart + paginationInfo.getRecordCountPerPage();
		map.put("START", intStart);
		map.put("END", intEnd);

		List listBBS_MST = mainService.Select_BBS_MST_MNG(map);
				
		if (listBBS_MST.size() == 0) {
			//TODO 데이터가 없다는 문구표현 필요?
			
		}else {
			int intTotalCNT = Integer.parseInt(((HashMap<String,Object>)(listBBS_MST.get(0))).get("TOTAL_CNT").toString());
			paginationInfo.setTotalRecordCount(intTotalCNT);
		}
		
		HashMap<String,Object> condition = new HashMap<>();
		condition.put("selectedItem", CONDITION);
		condition.put("searchedWord", SEARCH_WORD.trim());
		
		ModelAndView mav = new ModelAndView("jsonView");
		mav.setViewName(VIEW_NM);
		mav.addObject("data", listBBS_MST);
		mav.addObject("loginfo", loginfo);
		mav.addObject("condition", condition);
		mav.addObject(paginationInfo);

		return mav;
	}
	
	
	@RequestMapping("/actionBoardView.do")
	protected ModelAndView actionBoardView(@RequestParam("BBS_SEQ") String BBS_SEQ,
											  @RequestParam("VIEW_NM") String VIEW_NM) throws Exception {
		
		System.out.println("********** 게시판 글보기 **********");
		System.out.println("BBS_SEQ : " + BBS_SEQ);
		System.out.println("VIEW_NM : " + VIEW_NM);
		
		ModelAndView mav = new ModelAndView("jsonView");
		mav.setViewName(VIEW_NM);
		mav.addObject("condition", BBS_SEQ);
		
		HashMap map = new HashMap();
		map.put("BBS_SEQ", BBS_SEQ);
		
		List listBBS_View = mainService.Select_BBS_MST_MNG_View(map);
		
		
		if(listBBS_View.size()==1) {
			mav.addObject("data", listBBS_View.get(0));
			
			//조회 수 ++
			mainService.Update_BBS_MST_CNT(map);
		}
		
		return mav;
	}
	

	@RequestMapping("/actionBoardEdit.do")
	protected ModelAndView actionBoardEdit(@RequestParam("MNU_CD") String MNU_CD,
										   @RequestParam("ITEM_1") String ITEM_1,
										   @RequestParam(value="ITEM_2", required=false, defaultValue="") String ITEM_2,
										   @RequestParam("BBS_SEQ") String BBS_SEQ,
										   @RequestParam("VIEW_NM") String VIEW_NM) throws Exception{
		
		System.out.println("********** 게시판 등록&수정 **********");
		System.out.println("BBS_SEQ : " + BBS_SEQ);
		
		ModelAndView mav = new ModelAndView("jsonView");
		mav.setViewName(VIEW_NM);
		mav.addObject("condition", BBS_SEQ);
		
		//jsp 콤보박스 바인딩 데이터
		HashMap map = new HashMap();
		map.put("MST_CD", ITEM_1);
		List listITEM_1 = mainService.Select_CD_INFO(map);
		mav.addObject("ITEM_1", listITEM_1);
		
		//jsp 콤보박스_2 바인딩 데이터 (없을수도있음)
		if(!ITEM_2.equals("")) {
			map.clear();
			map.put("MST_CD", ITEM_2);
			List listITEM_2 = mainService.Select_CD_INFO(map);	
			mav.addObject("ITEM_2", listITEM_2);
		}
		
		//게시글 수정 일때
		if(!BBS_SEQ.equals("-1")) {
			map.put("BBS_SEQ", BBS_SEQ);
			
			List listBBS_View = mainService.Select_BBS_MST_MNG_View(map);
			
			if(listBBS_View.size()==1) {
				mav.addObject("data", listBBS_View.get(0));
			}
		}
		
		return mav;
	}
	
	@RequestMapping("/actionSave.do")
	protected ModelAndView actionSave(@RequestParam("MNU_CD") String MNU_CD,
									  @RequestParam("ITEM_1") String ITEM_1,
									  @RequestParam(value="ITEM_2", required=false, defaultValue="") String ITEM_2,
									  @RequestParam("BBS_SEQ") String BBS_SEQ,
									  @RequestParam("TTL") String TTL,
									  @RequestParam("CONTENTS") String CONTENTS,
									  HttpServletRequest request) throws Exception{

		System.out.println("********** 게시글 저장 **********");
		System.out.println("BBS_SEQ = " + BBS_SEQ);
		
		HashMap map = new HashMap();
		map.put("BBS_SEQ", BBS_SEQ); //신규 = -1로 처리
		map.put("BIZ_SEQ", "");
		map.put("MNU_CD", MNU_CD);
		map.put("ITEM_1", ITEM_1);
		map.put("ITEM_2", ITEM_2);
		map.put("TTL", TTL);
		map.put("CONTENTS", CONTENTS);
		map.put("EDT_ID", request.getSession().getAttribute("USER_ID").toString());
		
		int intSelectKey_BBS_SEQ = mainService.Merge_BBS_MST_MNG(map);
		
		ModelAndView mav = actionBoardView(Integer.toString(intSelectKey_BBS_SEQ),"noticeView");
		
		return mav;
	}
	
	@RequestMapping("/actionDelete.do")
	protected ModelAndView actionDelete(@RequestParam("MNU_CD") String MNU_CD,
										@RequestParam("VIEW_NM") String VIEW_NM,
										@RequestParam("BBS_SEQ") String BBS_SEQ,
										HttpServletRequest request) {
		try {
		HashMap map = new HashMap();
		map.put("BBS_SEQ", BBS_SEQ);
		
		mainService.Delete_BBS_MST_MNG(map);
		
		ModelAndView mav = actionBoard(MNU_CD,VIEW_NM,"1","*","",request);
		
		return mav;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.out.println("ERROR 발생 : " + e.toString());
			return null;
		}
		
	}
}
